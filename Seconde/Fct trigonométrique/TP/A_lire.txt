Ce fichier correspond � la fiche de cours trigo_act_cours.doc...


I D�couverte du cercle trigonom�trique et des radians:

Utiliser le fichier Enroulement.g2w (Aide � l'ouverture)


II Construction des repr�sentations graphiques de cosinus et sinus de mani�re interactive:

1. Ouvrir les deux fichiers enroul-trigo.g2w et cossin.g2w dans geoplanw

2. A l'aide du menu Fen�tre, afficher le fichier cercle.g2w
Vous pouvez bouger le point N sur la droite r�elle � l'aide des fl�ches haut et bas du pav� directionnel.

3. Dans le menu Fen�tre, choisir "mosa�que" 
=> le cercle est � gauche, les repr�sentations des fonctions seront � droite!

4. S�lectionner la fen�tre cossin.g2w, afficher au choix les points correspondants aux fonctions sin et cos
en appuyant respectivement sur les touches "s" et "c".

5. Appuyer sur le bouton "T" 
(ou dans le menu Afficher: "mode trace (bascule)")

6. S�lectionner la fen�tre cercle.g2w, et bouger le point N (haut et bas du pav�).

Voil�! Les jolies courbes demand�es (cos et (ou) sin) se tracent toutes seules... :p

